package ru.matveev.alexey.prometheus.jira.extension.metrics.requestmetric;

import com.anovaapps.atlassian.prometheus.spi.api.PrometheusRequestMetric;
import com.yevdo.jwildcard.JWildcard;
import com.yevdo.jwildcard.JWildcardRule;
import com.yevdo.jwildcard.JWildcardRules;
import io.prometheus.client.Histogram;

import javax.inject.Named;
import javax.servlet.ServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Named
public class RequestHistogram extends PrometheusRequestMetric {
    Histogram simpleRequestMetric = Histogram.build()
            .buckets(2, 4, 6, 8, 10, 12, 14, 16, 18, 20)
            .name("simple_request_metric")
            .help("Request duration on path")
            .labelNames("path")
            .create();

    private final String [] checkedUrls = {
            "/dosearchsite.action",
            "/pages/viewpage.action",
            "/rest/api/content*?status=",
            "/secure/CreateIssue",
            "/secure/QuickCreateIssue",
            "/secure/AjaxIssueEditAction",
            "/rest/greenhopper/1.0/xboard/issue/details",
            "/secure/rapidboard.jspa*rapidview",
            "/browse/*filter=",
            "/browse/*jql=",
            "/issues/*filter=",
            "/issues/*jql=",
            "/rest/issueNav/1/IssueTable",
            "/secure/QueryComponent",
            "/rest/greenhopper/1.0/rapid/charts/",
            "/secure/RapidBoard.jspa*view=reporting"
    };
    public RequestHistogram() {
        super("simple_request_metric", "Request duration on path", true, true);
    }


    @Override
    public Histogram.Timer startTimer(String path, String queryString, ServletRequest request) {
        String labelPath = this.getPathToCollect(path, queryString);
        return !labelPath.isEmpty() && isNotBlank(labelPath) ? simpleRequestMetric.labels(labelPath).startTimer() : null;
    }

    @Override
    public PrometheusMetricResult collect() {
        return new PrometheusMetricResult(simpleRequestMetric.collect());
    }

    private String getPathToCollect(String path, String queryString) {
        String[] addedUrls = this.checkedUrls;
        String pathInUpperCase = path.toUpperCase();
        JWildcardRules jWildcardRules = new JWildcardRules(new HashSet<>(Collections.singletonList(new JWildcardRule("*", ".*"))));
        String curMatch = "";
        int matchCount = 0;
        for (String addedUrl : addedUrls) {
            if (addedUrl.isEmpty()) {
                continue;
            }
            if (pathInUpperCase.equals(addedUrl.toUpperCase()) &&
                    matchCount < addedUrl.length()) {
                matchCount = addedUrl.length();
                curMatch = addedUrl;
            }

            Pattern pattern = Pattern.compile(JWildcard.wildcardToRegex(addedUrl.toUpperCase(), jWildcardRules, true));
            Matcher matcher = pattern.matcher(queryString != null && !queryString.isEmpty() ? pathInUpperCase + "?" + queryString.toUpperCase() : pathInUpperCase);
            if (matcher.matches() &&
                    matchCount < addedUrl.length()) {
                matchCount = addedUrl.length();
                curMatch = addedUrl;
            }


        }
        if (matchCount > 0) {
            return curMatch;
        }

        return "";
    }
}

