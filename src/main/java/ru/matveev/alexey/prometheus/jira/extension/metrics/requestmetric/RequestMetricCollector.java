package ru.matveev.alexey.prometheus.jira.extension.metrics.requestmetric;

import com.anovaapps.atlassian.prometheus.spi.api.PrometheusMetricCollector;

public class RequestMetricCollector extends PrometheusMetricCollector {

    public RequestMetricCollector(RequestHistogram requestHistogram) {
        this.getMetrics().add(requestHistogram);
    }
}
